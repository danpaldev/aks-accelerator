# 09 - Troubleshooting Advice

## 9.0 Introduction

When problems occur, we can use Azure Log Analytics to help diagnose problems with our AKS configuration.


## 9.1 Enabling Diagnostic Logging from AKS

If you have applied the Terraform Configurations in this project, you will already have Diagnostic logging enabled for:

- kube-scheduler
- kube-apiserver
- kube-controller-manager

Logs from these components will be visible in the Log Analytics Workspace that was also created by Terraform.

### 9.1.1 Using Log Analytics

To query these logs, we browse to the `aks-accelerator-law` Log Analytics Workspace in portal.azure.com (I don't believe this is possible from Azure CLI).

![Log Analytics Workspace](./img/10_log_analytics_diagnostics.png "Log Analytics workspace")

We can then query the `AzureDiagnostics` object:

```
AzureDiagnostics
| order by TimeGenerated desc nulls first 
| where log_s contains "error" 
| project log_s 
```

Some of the errors we find are quite explicit, for example:

> Note: the message below has been edited for brevity.

```
Message=\"The client '<some client id>' with object id '<some client id>'
has permission to perform action 'Microsoft.Network/networkInterfaces/write'
on scope '</path/to>/aks-kscclrtr-40561189-nic-0';
however, it does not have permission to perform action 'Microsoft.Network/virtualNetworks/subnets/join/action'
on the linked scope(s) '</path/to>/aks-accelerator-vnet-sn1'
```

### 9.1.2 Enabling Log Analytics Manually

To enable Log Analytics manually (if you didn't use these Terraform configurations):

- Navigate to the Resource Group that contains the AKS Cluster (e.g. `aks-accelerator-rg`)
- Navigate to the `Diagnostic Settings` blade
- Click through to Enable diagnostics, creating a Storage Account if a suitable does not already exist. 

## 9.2 Troubleshooting Gitlab-AKS Connectivity

### 9.2.1 Authentication Troubleshooting
Authentication with AKS is currently subject to a few pending Gitlab enhancements/bugs:

- [This Gitlab Issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/55362) covers issues related to AKS authentication (in particular when faced with missing KUBE environment variables)
- [The Gitlab documentation](https://gitlab.com/help/user/project/clusters/index#troubleshooting-missing-kubeconfig-or-kube_token) also has some troubleshooting tips (suggestions of possible causes)

### 9.2.2 Helm Installation Troubleshooting
Installation of Tiller (the first step when attempting to use Gitlab with AKS) can also be difficult to troubleshoot.

- [This Gitlab thread](https://forum.gitlab.com/t/kubernetes-integration-something-went-wrong-while-installing-helm-tiller/15627) gives some advice as to what to try when faced with the message _"Something went wrong while installing Helm Tiller"_.

A fix for that issue is to run the following (as per [this Github issue](https://github.com/helm/helm/issues/3985)):

```
kubectl -n kube-system patch deployment tiller-deploy -p '{"spec": {"template": {"spec": {"automountServiceAccountToken": true}}}}'
```



### 9.2.3 Troubleshooting Failed Deployments
For role/permission based issues, the `kubectl auth can-i` command is useful.

For example, if Gitlab was to report an error such as the following:

```
User "system:serviceaccount:gitlab-managed-apps:default"
cannot get resource "deployments" in API group "apps"
in the namespace "gitlab-managed-apps"
```

We can troubleshoot it by checking the permissions available to that user:

```
kubectl auth can-i --as system:serviceaccount:gitlab-managed-apps:default --all-namespaces=true get deployments
```

After changing rolebindings, we can then check again (to see if `can-i` now returns `yes` instead of `no`).


## 9.3 Troubleshooting Azure Load Balancers

### 9.3.1 Failure to Provision an IP Address
If Azure fails to provision a Load Balancer when exposing a service, check that:

- You have not specified a `LoadBalancerIP` that is outside of the AKS subnet address space
- The Service Principal being used by AKS has the _Network Contributor_ role


< [08 Deploying a Container Image with Gitlab CI/CD](08_gitlab_ci_deploy_image.md) | 10 Troubleshooting | 
